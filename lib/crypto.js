module.exports = class CryptoUtils {

  constructor(config) {
    this.config = config || {};
  }

  static create(config){
    return new CryptoUtils(config);
  }

  randomBytes(len) {
    const { randomBytes } = require('crypto');
    return randomBytes(len);
  }

  encryptStringSymmetrical({data, secret, iv, algorithm}){

    if (!secret) throw new Error('secret argument is required for this operation');
    if (!iv) throw new Error('iv argument is required for this operation');
    if (!data) throw new Error('data argument is required for this operation');
    if (!algorithm) algorithm = 'aes-256-ctr';

    const { createCipheriv } = require('crypto');

    var cipher = createCipheriv(algorithm, secret, iv);
    return cipher.update(data, 'utf8', 'hex') + cipher.final('hex');
  }

  decryptStringSymmetrical({encrypted, secret, iv, algorithm}){
    if (!secret) throw new Error('secret argument is required for this operation');
    if (!iv) throw new Error('iv argument is required for this operation');
    if (!encrypted) throw new Error('encrypted argument is required for this operation');
    if (!algorithm) algorithm = 'aes-256-ctr';

    const { createDecipheriv } = require('crypto');

    var decipher = createDecipheriv(algorithm, secret, iv);
    return decipher.update(encrypted, 'hex', 'utf8') + decipher.final('utf8');
  }

  encryptObjectSymmetrical({secret, data, iv, algorithm}){
    return this.encryptStringSymmetrical({data:JSON.stringify(data), secret, iv, algorithm});
  }

  decryptObjectSymmetrical({secret, encrypted, iv, algorithm}){
    return JSON.parse(this.decryptStringSymmetrical({encrypted, secret, iv, algorithm}));
  }

  hash({type, secret, value}){
    if (!type) type = 'sha256';
    const { createHmac, createHash } = require('crypto');
    var toHash = value || Date.now().toString();
    if (typeof value == 'object') toHash = JSON.stringify(value);

    if (secret) return createHmac(type, secret)
                       .update(toHash)
                       .digest('hex');

    return createHash(type)
           .update(toHash)
           .digest('hex');
  }



  sign({privateKey, data, passphrase}){

    if (!passphrase) throw new Error('passphrase is not optional');
    if (!privateKey) throw new Error('privateKey is not optional');
    if (!data) throw new Error('data is not optional');

    const { createSign, createPrivateKey } = require('crypto');

    const sign = createSign('SHA256');
    sign.write(data);
    sign.end();

    return sign.sign(createPrivateKey({
      key:privateKey,
      passphrase
    }), 'hex');
  }

  verify({publicKey, data, signature}){
    if (!signature) throw new Error('signature is not optional');
    if (!publicKey) throw new Error('publicKey is not optional');
    if (!data) throw new Error('data is not optional');
    const { createVerify } = require('crypto');
    const verify = createVerify('SHA256');
    verify.write(data);
    verify.end();
    return verify.verify(publicKey, signature, 'hex');
  }

  encryptObjectAsymmetrical({publicKey, obj, format}){
    if (!publicKey) throw new Error('publicKey is not optional');
    if (!obj) throw new Error('obj is not optional');
    if (!format) format = 'base64';
    const { publicEncrypt } = require('crypto');
    const toEncrypt = Buffer.from(JSON.stringify(obj));
    return publicEncrypt(publicKey, toEncrypt).toString(format);
  }

  decryptObjectAsymmetrical({privateKey, encrypted, passphrase, format}){

    if (!privateKey) throw new Error('privateKey is not optional');
    if (!passphrase) throw new Error('passphrase is not optional');
    if (!encrypted) throw new Error('encrypted is not optional');
    if (!format) format = 'base64';

    const { privateDecrypt } = require('crypto');
    let buffer = Buffer.from(encrypted, format);

    const decrypted = privateDecrypt(
        {
            key: privateKey,
            passphrase: passphrase,
        },
        buffer
    );
    return JSON.parse(decrypted.toString());
  }

  keyPair({type, options, passphrase}){

    if (!passphrase) throw new Error('passphrase is not optional');
    if (!type) type = 'rsa';

    if (!options) options = {
      modulusLength: 4096,
      namedCurve: 'secp256k1',
      publicKeyEncoding: {
          type: 'spki',
          format: 'pem'
      },
      privateKeyEncoding: {
          type: 'pkcs8',
          format: 'pem',
          cipher: 'aes-256-cbc',
          passphrase: passphrase
      }
    };

    const { generateKeyPairSync } = require('crypto');
    return generateKeyPairSync(type, options);
  }
};

module.exports = class Logger {

  constructor(config){
    this.log4js = require('log4js');
    this.utils = require('..').Utils.create();
    this.config = this.defaults(config);
    this.log4js.configure(this.config);
    this.logger = this.log4js.getLogger();
  }

  static create(config){
    return new Logger(config);
  }

  info(msg){
    this.logger.info(msg);
  }

  debug(msg){
    this.logger.debug(msg);
  }

  error(e){
    this.logger.error(e.toString());
  }

  warn(msg){
    this.logger.warn(msg);
  }

  defaults(config){
    if (!config) config = {};
    var cloned = this.utils.clone(config);
    if (!cloned.level) cloned.level = 'info';
    if (!cloned.appenders) cloned.appenders = {};
    if (!cloned.appenders.out) cloned.appenders.out = { type: 'stdout' };
    if (!cloned.categories) cloned.categories = {};
    if (!cloned.categories.default) cloned.categories.default = { appenders: ['out'], level: cloned.level };
    return cloned;
  }
};

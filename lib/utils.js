module.exports = class Utils {

  constructor(
    config
  ) {
    this.fs = require('fs-extra');
    this.rimraf = require("rimraf");
    this.crypto = require('..').Crypto.create(config);
  }

  static create(config) {
    return new Utils(config);
  }

  clone(item) {
    if (!item) return item;
    return JSON.parse(JSON.stringify(item));
  }

  getFiles(folder) {
    let contents = this.getFolderContents(folder, {
      withFileTypes: true
    });
    return contents.filter(function(filespec) {
      return !filespec.isDirectory();
    });
  }

  getDirectories(folder) {
    let contents = this.getFolderContents(folder, {
      withFileTypes: true
    });
    return contents.filter(function(filespec) {
      return filespec.isDirectory();
    });
  }

  getFolderContents(folder, opts) {
    return this.fs.readdirSync(folder, opts);
  }

  getConstructorArgumentNamesFromPath(requirePath, className) {
    try {
      let requiredClass = className != null?require(requirePath)[className]:require(requirePath);
      return this.getConstructorArgumentNamesFromClass(requiredClass);
    } catch (e) {
      throw new Error('Failed getting constructor argument names: ' + e.message);
    }
  }

  getConstructorArgumentNamesFromClass(requiredClass) {
    try {
      const args = new RegExp('constructor\\s*\\((.*?)\\)')
        .exec(requiredClass.prototype.constructor.toString().replace(/\n/g, ''))[1]
        .replace(/\/\*.*?\*\//g, '')
        .replace(/ /g, '')
        .split(',');

      return args.filter((arg) => {
        return arg != "";
      });
    } catch (e) {
      throw new Error('Failed getting constructor argument names: ' + e.message);
    }
  }

  fileDelete(path) {
    return this.fs.unlinkSync(path);
  }

  fileReadData(path) {
    try{
        return this.fs.readFileSync(path);
    }catch(e){
      if (e.message.indexOf('ENOENT: no such file or directory') > -1) return null;
      throw e;
    }
  }

  fileReadDataObject(path) {
    const fileData = this.fileReadData(path);
    if (fileData == null) return null;
    return JSON.parse(fileData.toString());
  }

  fileWriteObject(path, obj, append) {
    if (append) return this.fs.writeFileSync(path, JSON.stringify(obj), {flag: 'a'});
    this.fs.writeFileSync(path, JSON.stringify(obj));
  }

  fileExists(path) {
    try {
      return this.fs.statSync(path).isFile();
    } catch (e) {
      return false;
    }
  }

  folderExists(path) {
    try {
      return this.fs.statSync(path).isDirectory();
    } catch (e) {
      return false;
    }
  }

  folderEnsure(path){
    try{
      return this.fs.ensureDirSync(path);
    } catch(e){
      throw new Error(`failed ensuring folder ${path}: ` + e.message);
    }
  }

  folderDelete(path){
    try{
      return this.rimraf.sync(path);
    } catch(e){
      throw new Error(`failed deleting folder ${path}: ` + e.message);
    }
  }
};

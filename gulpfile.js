var gulp = require('gulp');
var jshint = require('gulp-jshint');

var lintSrc = [
  './lib/**/*.js'
];

gulp.task('lint', function() {
  return gulp.src(lintSrc)
    .pipe(jshint({ esversion: 9 }))
    .pipe(jshint.reporter('default'));
});

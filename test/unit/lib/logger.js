const helper = require('../../__fixtures/test-helper').create();
const path = require('path');
const _expect = require('expect.js');

describe(helper.testName(__filename, 4), function() {

  it('test the various logger functions', () => {
    let logger = require('plebs').Logger.create();
    logger.info('test info');
    logger.error(new Error('test error, actual'));
    logger.error('test error, string');
    logger.warn('test warn');
    logger.debug('test debug');
  });
});

const helper = require('../../__fixtures/test-helper').create();
const _expect = require('expect.js');
const path = require('path');

describe(helper.testName(__filename, 4), function() {

  it('tests the getConstructorArgumentNamesFromClass method', () => {
    let utils = require('plebs').Utils.create();
    var constructorArgs = utils.getConstructorArgumentNamesFromClass(require('../../..').Utils);
    _expect(constructorArgs).to.eql(['config']);
  });

  it('tests the clone method', () => {
    let utils = require('plebs').Utils.create();
    let toClone = {test:'data'};
    let cloned = utils.clone(toClone);
    _expect(toClone).to.eql(cloned);
    _expect(toClone).to.not.equal(cloned);
    _expect(utils.clone(null)).to.eql(null);
    _expect(utils.clone(undefined)).to.eql(undefined);
  });

  it('tests file and folder utility functions', () => {
    let utils = require('plebs').Utils.create();
    let testFolderPath = path.resolve(__dirname, '../../__tmp');
    utils.folderDelete(testFolderPath);
    _expect(utils.folderExists(testFolderPath)).to.be(false);
    let testFilePath = `${testFolderPath}${path.sep}test-file${Date.now()}.txt`;
    utils.folderEnsure(testFolderPath);
    _expect(utils.folderExists(testFolderPath)).to.be(true);
    utils.fileWriteObject(testFilePath, {test:'data'});
    _expect(JSON.parse(utils.fileReadData(testFilePath))).to.eql({test:'data'});
    _expect(utils.fileExists(testFilePath)).to.be(true);
    utils.fileDelete(testFilePath);
    _expect(utils.fileExists(testFilePath)).to.be(false);
    _expect(utils.fileReadData(testFilePath)).to.eql(null);
    utils.folderDelete(testFolderPath);
    _expect(utils.folderExists(testFolderPath)).to.be(false);
  });
});

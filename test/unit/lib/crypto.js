const helper = require('../../__fixtures/test-helper').create();
const path = require('path');
const _expect = require('expect.js');

describe(helper.testName(__filename, 4), function() {

  this.timeout(10000);

  it('tests the randomBytes function', () => {
    let crypto = require('plebs').Crypto.create();
    let randomBytes = crypto.randomBytes(10);
    _expect(randomBytes).to.not.be(null);
    _expect(randomBytes.length).to.be(10);
  });

  it('tests the encryptStringSymmetrical and decryptStringSymmetrical functions', () => {
    let crypto = require('plebs').Crypto.create();
    let encrypted, decrypted;
    let secret = crypto.randomBytes(32);
    let iv = crypto.randomBytes(16);
    let data = 'test-data';
    let errorsCount = 0;

    try{
      encrypted = crypto.encryptStringSymmetrical({});
    }catch(e){
      _expect(e.message).to.be('secret argument is required for this operation');
      errorsCount++;
    }

    try{
      encrypted = crypto.encryptStringSymmetrical({secret});
    }catch(e){
      _expect(e.message).to.be('iv argument is required for this operation');
      errorsCount++;
    }

    try{
      encrypted = crypto.encryptStringSymmetrical({secret, iv});
    }catch(e){
      _expect(e.message).to.be('data argument is required for this operation');
      errorsCount++;
    }

    encrypted = crypto.encryptStringSymmetrical({secret, iv, data});

    try{
      decrypted = crypto.decryptStringSymmetrical({});
    }catch(e){
      _expect(e.message).to.be('secret argument is required for this operation');
      errorsCount++;
    }

    try{
      decrypted = crypto.decryptStringSymmetrical({secret});
    }catch(e){
      _expect(e.message).to.be('iv argument is required for this operation');
      errorsCount++;
    }

    try{
      decrypted = crypto.decryptStringSymmetrical({secret, iv});
    }catch(e){
      _expect(e.message).to.be('encrypted argument is required for this operation');
      errorsCount++;
    }

    decrypted = crypto.decryptStringSymmetrical({secret, iv, encrypted});
    _expect(decrypted).to.be('test-data');
    _expect(errorsCount).to.be(6);
  });

  it('tests the encryptObjectSymmetrical and decryptObjectSymmetrical functions', () => {
    let crypto = require('plebs').Crypto.create();
    let encrypted, decrypted;
    let secret = crypto.randomBytes(32);
    let iv = crypto.randomBytes(16);
    let data = {test:'data'};
    let errorsCount = 0;

    try{
      encrypted = crypto.encryptObjectSymmetrical({});
    }catch(e){
      _expect(e.message).to.be('secret argument is required for this operation');
      errorsCount++;
    }

    try{
      encrypted = crypto.encryptObjectSymmetrical({secret});
    }catch(e){
      _expect(e.message).to.be('iv argument is required for this operation');
      errorsCount++;
    }

    try{
      encrypted = crypto.encryptObjectSymmetrical({secret, iv});
    }catch(e){
      _expect(e.message).to.be('data argument is required for this operation');
      errorsCount++;
    }

    encrypted = crypto.encryptObjectSymmetrical({secret, iv, data});

    try{
      decrypted = crypto.decryptObjectSymmetrical({});
    }catch(e){
      _expect(e.message).to.be('secret argument is required for this operation');
      errorsCount++;
    }

    try{
      decrypted = crypto.decryptObjectSymmetrical({secret});
    }catch(e){
      _expect(e.message).to.be('iv argument is required for this operation');
      errorsCount++;
    }

    try{
      decrypted = crypto.decryptObjectSymmetrical({secret, iv});
    }catch(e){
      _expect(e.message).to.be('encrypted argument is required for this operation');
      errorsCount++;
    }

    decrypted = crypto.decryptObjectSymmetrical({secret, iv, encrypted});
    _expect(decrypted).to.eql({test:'data'});
    _expect(errorsCount).to.be(6);
  });

  it('tests the hash function', () => {
    let crypto = require('plebs').Crypto.create();
    let hash = crypto.hash({});
    _expect(hash.length).to.be(64);
    hash = crypto.hash({value:'test'});
    _expect(hash).to.be('9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08');
    hash = crypto.hash({value:'test', secret:'special'});
    _expect(hash).to.be('7bcaa888464e57ab009f61b070907c9f42490757c43868c2ad00d389c784be4b');
    hash = crypto.hash({value:{data:'test'}, secret:'special'});
    _expect(hash).to.be('61afdcca113180d76a703b8ce43e2e16c4a33196e134804f770b4a005b1e11cb');
  });

  it('tests the keyPair function', () => {
    let crypto = require('plebs').Crypto.create();
    let keyPair, passphrase = 'test-pass-phrase';
    let errorsCount = 0;
    try{
      keyPair = crypto.keyPair({});
    }catch(e){
      _expect(e.message).to.be('passphrase is not optional');
      errorsCount++;
    }
    keyPair = crypto.keyPair({passphrase});
    _expect(keyPair.privateKey).to.not.eql(null);
    _expect(keyPair.publicKey).to.not.eql(null);
    _expect(errorsCount).to.be(1);
  });

  it('tests the sign and verify functions', () => {
    let crypto = require('plebs').Crypto.create();
    let keyPair, badKeyPair, signature, passphrase = 'test-pass-phrase';
    let errorsCount = 0;
    keyPair = crypto.keyPair({passphrase});
    let privateKey = keyPair.privateKey;
    let publicKey = keyPair.publicKey;
    badKeyPair = crypto.keyPair({passphrase:'bad-pass-phrase'});
    let badPublicKey = badKeyPair.publicKey;
    let badPrivateKey = badKeyPair.privateKey;
    let data = crypto.hash({});

    try{
      signature = crypto.sign({});
    }catch(e){
      _expect(e.message).to.be('passphrase is not optional');
      errorsCount++;
    }

    try{
      signature = crypto.sign({passphrase});
    }catch(e){
      _expect(e.message).to.be('privateKey is not optional');
      errorsCount++;
    }

    try{
      signature = crypto.sign({passphrase, privateKey});
    }catch(e){
      _expect(e.message).to.be('data is not optional');
      errorsCount++;
    }

    signature = crypto.sign({passphrase, privateKey, data});

    let badSignature = crypto.sign({passphrase:'bad-pass-phrase', privateKey:badPrivateKey, data});

    try{
      crypto.verify({});
    }catch(e){
      _expect(e.message).to.be('signature is not optional');
      errorsCount++;
    }

    try{
      crypto.verify({signature});
    }catch(e){
      _expect(e.message).to.be('publicKey is not optional');
      errorsCount++;
    }

    try{
      crypto.verify({signature, publicKey});
    }catch(e){
      _expect(e.message).to.be('data is not optional');
      errorsCount++;
    }

    _expect(crypto.verify({signature, publicKey, data})).to.be(true);
    _expect(crypto.verify({signature, publicKey, data:crypto.hash({})})).to.be(false);
    _expect(crypto.verify({signature, publicKey:badPublicKey, data})).to.be(false);
    _expect(crypto.verify({signature:badSignature, publicKey, data})).to.be(false);

    _expect(errorsCount).to.be(6);
  });

  it('tests the encryptObjectAsymmetrical and decryptObjectAsymmetrical functions', () => {

    let crypto = require('plebs').Crypto.create();
    let keyPair, passphrase = 'test-pass-phrase';
    let errorsCount = 0;

    keyPair = crypto.keyPair({passphrase});
    let publicKey = keyPair.publicKey;
    let privateKey = keyPair.privateKey;
    let obj = {test:'data'};

    try{
      crypto.encryptObjectAsymmetrical({});
    }catch(e){
      _expect(e.message).to.be('publicKey is not optional');
      errorsCount++;
    }

    try{
      crypto.encryptObjectAsymmetrical({publicKey});
    }catch(e){
      _expect(e.message).to.be('obj is not optional');
      errorsCount++;
    }

    let encrypted = crypto.encryptObjectAsymmetrical({publicKey, obj});

    try{
      crypto.decryptObjectAsymmetrical({});
    }catch(e){
      _expect(e.message).to.be('privateKey is not optional');
      errorsCount++;
    }

    try{
      crypto.decryptObjectAsymmetrical({privateKey});
    }catch(e){
      _expect(e.message).to.be('passphrase is not optional');
      errorsCount++;
    }

    try{
      crypto.decryptObjectAsymmetrical({privateKey, passphrase});
    }catch(e){
      _expect(e.message).to.be('encrypted is not optional');
      errorsCount++;
    }

    _expect(crypto.decryptObjectAsymmetrical({privateKey, passphrase, encrypted})).to.eql(obj);
    _expect(errorsCount).to.be(5);
  });
});

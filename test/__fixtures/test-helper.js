var path = require('path');

function TestHelper() {}

TestHelper.create = function() {
  return new TestHelper();
};

TestHelper.prototype.testName = function(testFilename, depth) {

  if (!depth) depth = 2;

  var fileParts = testFilename.split(path.sep).reverse();

  var poParts = [];

  for (var i = 0; i < depth; i++)
    poParts.push(fileParts.shift());

  return poParts.reverse().join('/').replace('.js', '');
};

module.exports = TestHelper;
